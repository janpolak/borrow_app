import Vue from "vue";
import Router from "vue-router";
import BList from "./views/BList.vue";
import BOptions from "./views/BOptions.vue";
import BItem from "./views/BItem.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: BList,
      children: [
        {
          path: ":id",
          component: BItem
        }
      ]
    },
    {
      path: "/options",
      name: "options",
      component: BOptions
    }
  ]
});
