export default {
  namespaced: true,
  state: {
    dark: false,
    perPage: 12
  },
  getters: {
    dark(state) {
      return state.dark;
    },
    perPage(state) {
      return state.perPage;
    }
  },
  mutations: {
    dark(state, payload) {
      if (payload) {
        state.dark = !state.dark;
      } else {
        state.dark = payload;
      }
    },
    perPage(state, payload) {
      state.perPage = payload;
    }
  }
};
