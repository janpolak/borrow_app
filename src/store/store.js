//#region Imports
import Vue from "vue";
import Vuex from "vuex";
import { VuexPersistence } from "vuex-persist";
import data from "./data";
import options from "./options";
//#endregion

// Vuex setup
Vue.use(Vuex);

//#region Vuex-persist setup
const persisted = new VuexPersistence({
  key: "borrow_app",
  storage: window.localStorage,
  reducer: state => ({
    data: state.data,
    options: state.options
  })
});
//#endregion

export default new Vuex.Store({
  modules: {
    data: data,
    options: options
  },
  plugins: [persisted.plugin]
});
