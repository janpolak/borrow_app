export default {
  namespaced: true,
  state: {
    items: []
  },
  mutations: {
    store(state, payload) {
      payload.added = Date.now();
      state.items.push(payload);
    },
    edit(state, payload) {
      state.items[payload.id] = payload.data;
    }
  },
  getters: {
    item: state => payload => {
      return payload == -1
        ? {
            properties: [
              {
                title: "Item name",
                value: ""
              },
              {
                title: "Borrowed to",
                value: ""
              }
            ]
          }
        : state.items[payload];
    },

    items(state) {
      return state.items;
    }
  }
};
